import com.smirnowku.money.util.Money;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MoneyTest extends BaseTest {

    private final static Random RANDOM = new Random();

    private static Money randomMoney() {
        return new Money(RANDOM.nextInt(1000), RANDOM.nextInt(100));
    }

    @Test
    public void random() {
        Money a = randomMoney();
        Money b = randomMoney();
        print("a = " + a);
        print("b = " + b);
        print(a.compareTo(b) == 0 ? "a = b" : (a.compareTo(b) > 0 ? "a > b" : "a < b"));

        Money sum = a.add(b);
        Money sub = a.compareTo(b) > 0 ? a.subtract(b) : b.subtract(a);
        print("a + b = " + sum);
        print("|a - b| = " + sub);

        assertEquals(a.add(b), b.add(a));
        assertEquals(a.compareTo(b), -b.compareTo(a));
        assertTrue(a.add(b).add(b).subtract(b).add(a).subtract(b).subtract(a).equals(a));
    }

    @Test
    public void definite1() {
        Money a = new Money(123, 45);
        Money b = new Money(420, 87);
        assertTrue(a.compareTo(b) < 0);

        assertEquals(new Money(544, 32), a.add(b));
        assertEquals(new Money(297, 42), b.subtract(a));

        try {
            a.subtract(b);
            Assert.fail();
        } catch (Throwable e) {
            assertTrue(e.getClass() == RuntimeException.class);
        }
    }

    @Test
    public void definite2() {
        Money a = new Money(123, 45);
        Money b = new Money(420, 11);
        assertTrue(b.compareTo(a) > 0);

        assertEquals(new Money(543, 56), b.add(a));
        assertEquals(new Money(296, 66), b.subtract(a));
    }

    @Test
    public void hash() {
        Money a = randomMoney();
        Money b = randomMoney();
        Money a_copy = new Money(a.getMajorUnit(), a.getMinorUnit());
        Money b_copy = new Money(b.getMajorUnit(), b.getMinorUnit());
        HashSet<Money> money = new HashSet<>();

        money.add(a);
        assertEquals(1, money.size());
        assertTrue(money.contains(a));
        money.add(b);
        assertEquals(2, money.size());
        assertTrue(money.contains(b));
        money.remove(b);
        assertEquals(1, money.size());
        assertTrue(!money.contains(b));
        money.remove(a_copy);
        assertEquals(0, money.size());
        assertTrue(!money.contains(a));
        money.add(b_copy);
        assertEquals(1, money.size());
        assertTrue(money.contains(b));
        money.remove(b);
        assertEquals(0, money.size());
        assertTrue(!money.contains(b));
    }

    @Test
    public void zero() {
        assertEquals(new Money(0, 0), Money.zero());
        Money x = randomMoney();
        assertEquals(x.add(x).subtract(x).subtract(x), Money.zero());
    }
}
