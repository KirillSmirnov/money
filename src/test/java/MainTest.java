import com.smirnowku.money.core.AuthService;
import com.smirnowku.money.core.DataSourceManager;
import com.smirnowku.money.core.domain.*;
import com.smirnowku.money.util.Money;
import org.junit.After;
import org.junit.Test;

import java.io.File;

public class MainTest extends BaseTest {

    private static final String USERNAME = "test";
    private static final String PASSWORD = "pass";

    private final DataSourceManager dataSourceManager;
    private final String path0;
    private AuthService authService;

    public MainTest() {
        dataSourceManager = new DataSourceManager();
        path0 = dataSourceManager.getSourceFile();
        dataSourceManager.openFile("./test.data");
        authService = dataSourceManager.provideAuthService();
    }

    @After
    public void revert() {
        dataSourceManager.closeFile();
        if (path0 != null) dataSourceManager.openFile(path0);
    }

    @Test
    public void start() {
        boolean hasUser = authService.hasUser();
        if (hasUser) {
            print("User: " + authService.getUsername());
            authService.authenticate(PASSWORD);
            if (authService.isAuthenticated()) {
                print("logged in");
                onLoggedIn();
                return;
            }
            print("logging in failed");
        } else {
            print("No user");
        }
        createUser();
    }

    private void createUser() {
        if (authService.hasUser()) {
            new File(dataSourceManager.getSourceFile()).delete();
            authService = dataSourceManager.provideAuthService();
        }
        authService.createUser(USERNAME, PASSWORD);
        onLoggedIn();
    }

    private void onLoggedIn() {
        User user = authService.getUser();
        print("Logged in as " + user.getUsername());

        deleteAllAccounts(user);

        user.addAccount("Bank010011", Currency.RUB);
        user.addAccount("Cash", Currency.RUB);
        user.addAccount("$$$", Currency.USD);
        user.getAccounts().get(1).addMoney("add", new Money(1000, 0));
        user.getAccounts().get(1).withdrawMoney("wd", new Money(109, 23));
        user.getAccounts().get(2).addMoney("add", new Money(987654, 91));
        user.getAccounts().get(2).addMoney("add", new Money(38943, 53));
        user.getAccounts().get(2).addMoney("add", new Money(8781, 0));
        user.getAccounts().get(2).addMoney("add", new Money(98365, 3));

        printAccountsAllTypes(user);
    }

    private void printAccountsAllTypes(User user) {
        print("\nAccounts of " + user.getUsername() + "::");
        user.setAccountsSortingType(AccountsSortingType.DATE);
        printAccounts(user);
        user.setAccountsSortingType(AccountsSortingType.CURRENCY);
        printAccounts(user);
        user.setAccountsSortingType(AccountsSortingType.NAME);
        printAccounts(user);
    }

    private void printAccounts(User user) {
        print("\nSorting type: " + user.getAccountsSortingType());
        if (user.getAccounts().isEmpty())
            print("<EMPTY>");
        for (Account account : user.getAccounts()) {
            print(account.toString());
            printRecords(account);
        }
    }

    private void printRecords(Account account) {
        print("\tRecords of account " + account.getName() + ":");
        if (account.getRecords().isEmpty())
            print("\t<EMPTY>");
        for (Record record : account.getRecords()) {
            print(String.format("\t%s %s %s %s %s", record.getDateTime(), record.getOperation(), record.getComment(), record.getAmount(), record.getBalance()));
        }
    }

    private void deleteAllAccounts(User user) {
        while (!user.getAccounts().isEmpty()) {
            user.getAccounts().get(0).delete();
        }
    }
}
