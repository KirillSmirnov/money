package com.smirnowku.money.core;

import com.smirnowku.money.core.domain.User;

public interface AuthService {

    boolean hasUser();

    boolean isAuthenticated();

    void createUser(String username, String password);

    String getUsername();

    void authenticate(String password);

    User getUser();
}
