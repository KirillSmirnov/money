package com.smirnowku.money.core;

import com.smirnowku.money.core.domain.User;

class UserServiceImpl implements UserService {

    private final Repository repository;

    UserServiceImpl(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void save(User user) {
        repository.setUser(user);
    }
}
