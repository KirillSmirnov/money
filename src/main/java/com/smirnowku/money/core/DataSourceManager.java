package com.smirnowku.money.core;

import com.smirnowku.money.UserPreferences;

public class DataSourceManager {

    private static final UserPreferences USER_PREFERENCES = new UserPreferences();
    private AuthService authService = createAuthService();

    public boolean isFileOpened() {
        return authService != null;
    }

    public void openFile(String sourceFile) {
        USER_PREFERENCES.setSourceFile(sourceFile);
        authService = createAuthService();
    }

    public void closeFile() {
        USER_PREFERENCES.setSourceFile(null);
        authService = null;
    }

    public AuthService provideAuthService() {
        if (!isFileOpened())
            throw new RuntimeException("AuthService not initialized");
        return authService;
    }

    public String getSourceFile() {
        return USER_PREFERENCES.getSourceFile();
    }

    private AuthService createAuthService() {
        String sourceFile = getSourceFile();
        return sourceFile == null ? null : CoreConfigurator.provideAuthService(sourceFile);
    }
}
