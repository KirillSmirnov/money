package com.smirnowku.money.core;

import com.smirnowku.money.core.domain.User;

interface Repository {

    User getUser();

    void setUser(User user);
}
