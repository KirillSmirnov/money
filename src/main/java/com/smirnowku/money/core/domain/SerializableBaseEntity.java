package com.smirnowku.money.core.domain;

import java.io.Serializable;

abstract class SerializableBaseEntity extends BaseEntity implements Serializable {
}
