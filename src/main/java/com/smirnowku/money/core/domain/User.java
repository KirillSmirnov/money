package com.smirnowku.money.core.domain;

import com.smirnowku.money.core.UserService;
import com.smirnowku.money.util.Money;
import com.smirnowku.money.util.NullChecker;

import java.util.*;
import java.util.stream.Collectors;

public class User extends SerializableBaseEntity {

    private transient UserService userService;

    private String username;
    private String password;
    private final List<Account> accounts;
    private AccountsSortingType accountsSortingType;

    public User(String username, String password) {
        NullChecker.ensureNotNullOrEmpty(username);
        NullChecker.ensureNotNull(password);
        this.username = username;
        this.password = password;
        this.accounts = new ArrayList<>();
        this.accountsSortingType = AccountsSortingType.DEFAULT;
    }

    public void addAccount(String name, Currency currency) {
        accounts.add(new Account(this, name, currency));
        update();
    }

    public void setAccountsSortingType(AccountsSortingType accountsSortingType) {
        NullChecker.ensureNotNull(accountsSortingType);
        this.accountsSortingType = accountsSortingType;
        update();
    }

    public void setUsername(String username) {
        NullChecker.ensureNotNullOrEmpty(username);
        this.username = username;
        update();
    }

    public void setPassword(String password) {
        NullChecker.ensureNotNull(password);
        this.password = password;
        update();
    }

    public AccountsSortingType getAccountsSortingType() {
        return accountsSortingType;
    }

    public String getUsername() {
        return username;
    }

    public List<Account> getAccounts() {
        return Collections.unmodifiableList(accounts);
    }

    public String getTotalBalance() {
        Queue<String> total = new LinkedList<>();
        Arrays.stream(Currency.values())
                .forEach(currency -> addTotalBalanceOfCurrency(total, currency));
        if (total.isEmpty()) return Money.zero().toString();
        return String.join(", ", total);
    }

    public void setUserService(UserService userService) {
        NullChecker.ensureNotNull(userService);
        this.userService = userService;
    }

    public boolean verifyPassword(String password) {
        return this.password.equals(password);
    }

    void deleteAccount(Account account) {
        accounts.remove(account);
        update();
    }

    void update() {
        sortAccounts();
        if (userService != null)
            userService.save(this);
    }

    private void sortAccounts() {
        Comparator<? super Account> comparator = null;
        if (accountsSortingType == AccountsSortingType.DATE) {
            comparator = Comparator.comparing(Account::getUpdated).reversed();
        } else if (accountsSortingType == AccountsSortingType.CURRENCY) {
            comparator = Comparator.comparing(Account::getCurrency)
                    .thenComparing(Account::getBalance).reversed();
        } else if (accountsSortingType == AccountsSortingType.NAME) {
            comparator = Comparator.comparing(Account::getName, String::compareToIgnoreCase);
        }
        accounts.sort(comparator);
    }

    private void addTotalBalanceOfCurrency(Queue<String> total, Currency currency) {
        Set<Account> accountsOfCurrency = accounts.stream()
                .filter(account -> account.getCurrency() == currency)
                .collect(Collectors.toSet());
        if (!accountsOfCurrency.isEmpty())
            total.add(accountsOfCurrency.stream()
                    .map(Account::getBalance)
                    .reduce(Money::add)
                    .orElse(Money.zero())
                    + " " + currency);
    }
}
