package com.smirnowku.money.core.domain;

import com.smirnowku.money.util.Money;
import com.smirnowku.money.util.NullChecker;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Account extends SerializableBaseEntity {

    private final User user;

    private String name;
    private final Currency currency;
    private final List<Record> records;

    Account(User user, String name, Currency currency) {
        NullChecker.ensureNotNull(user);
        NullChecker.ensureNotNullOrEmpty(name);
        NullChecker.ensureNotNull(currency);
        this.user = user;
        this.name = name;
        this.currency = currency;
        this.records = new ArrayList<>();
    }

    public void delete() {
        user.deleteAccount(this);
    }

    public void addMoney(String comment, Money amount) {
        addRecord(Operation.ADDING, comment, amount, getBalance().add(amount));
    }

    public boolean withdrawMoney(String comment, Money amount) {
        try {
            addRecord(Operation.WITHDRAWAL, comment, amount, getBalance().subtract(amount));
        } catch (RuntimeException e) {
            return false;
        }
        return true;
    }

    public void deleteLastRecord() {
        if (!records.isEmpty()) {
            records.remove(0);
            update();
        }
    }

    public void setName(String name) {
        NullChecker.ensureNotNullOrEmpty(name);
        this.name = name;
        update();
    }

    public String getName() {
        return name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public List<Record> getRecords() {
        return Collections.unmodifiableList(records);
    }

    Money getBalance() {
        return records.isEmpty() ? Money.zero() : records.get(0).getBalance();
    }

    LocalDateTime getUpdated() {
        return records.isEmpty() ? LocalDateTime.MIN : records.get(0).getDateTime();
    }

    void update() {
        user.update();
    }

    private void addRecord(Operation operation, String comment, Money amount, Money balance) {
        records.add(0, new Record(this, LocalDateTime.now(), operation, comment, amount, balance));
        update();
    }

    @Override
    public String toString() {
        return String.format("%s\n%s %s", getName(), getBalance(), getCurrency());
    }
}
