package com.smirnowku.money.core.domain;

public enum Operation {

    ADDING("+"),
    WITHDRAWAL("—");

    private final String symbol;

    Operation(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
