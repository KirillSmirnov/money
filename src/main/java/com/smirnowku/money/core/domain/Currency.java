package com.smirnowku.money.core.domain;

public enum Currency {

    RUB("\u20BD", "k."),
    USD("$", "¢"),
    EUR("€", "c");

    private final String majorUnitSymbol;
    private final String minorUnitSymbol;

    Currency(String majorUnitSymbol, String minorUnitSymbol) {
        this.majorUnitSymbol = majorUnitSymbol;
        this.minorUnitSymbol = minorUnitSymbol;
    }

    public String getMajorUnitSymbol() {
        return majorUnitSymbol;
    }

    public String getMinorUnitSymbol() {
        return minorUnitSymbol;
    }

    @Override
    public String toString() {
        return majorUnitSymbol;
    }
}
