package com.smirnowku.money.core.domain;

public enum AccountsSortingType {

    NAME,
    DATE,
    CURRENCY;

    public static final AccountsSortingType DEFAULT = NAME;
}
