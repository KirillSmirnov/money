package com.smirnowku.money.core.domain;

import com.smirnowku.money.util.Money;
import com.smirnowku.money.util.NullChecker;

import java.time.LocalDateTime;

public class Record extends SerializableBaseEntity {

    private final Account account;

    private final LocalDateTime dateTime;
    private final Operation operation;
    private String comment;
    private final Money amount;
    private final Money balance;

    Record(Account account, LocalDateTime dateTime, Operation operation, String comment, Money amount, Money balance) {
        NullChecker.ensureNotNull(account);
        NullChecker.ensureNotNull(dateTime);
        NullChecker.ensureNotNull(operation);
        NullChecker.ensureNotNull(amount);
        NullChecker.ensureNotNull(balance);
        this.account = account;
        this.dateTime = dateTime;
        this.operation = operation;
        this.comment = comment;
        this.amount = amount;
        this.balance = balance;
    }

    public void setComment(String comment) {
        this.comment = comment;
        update();
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Operation getOperation() {
        return operation;
    }

    public String getComment() {
        return comment;
    }

    public Money getAmount() {
        return amount;
    }

    public Money getBalance() {
        return balance;
    }

    private void update() {
        account.update();
    }
}
