package com.smirnowku.money.core;

import com.smirnowku.money.core.domain.User;
import com.smirnowku.money.serialization.DeserializationException;
import com.smirnowku.money.serialization.Serializer;

import java.io.File;

class RepositoryImpl implements Repository {

    private final File dataSource;
    private final Serializer serializer;
    private User user;

    RepositoryImpl(File dataSource, Serializer serializer) {
        this.dataSource = dataSource;
        this.serializer = serializer;
        readData();
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
        writeData();
    }

    private void readData() {
        try {
            user = serializer.deserialize(dataSource);
        } catch (DeserializationException e) {
            user = null;
        }
    }

    private void writeData() {
        serializer.serialize(user, dataSource);
    }
}
