package com.smirnowku.money.core;

import com.smirnowku.money.serialization.Serializer;
import com.smirnowku.money.serialization.SerializerConfigurator;

import java.io.File;

class CoreConfigurator {

    static AuthService provideAuthService(String sourceFile) {
        Repository repository = provideRepository(sourceFile);
        return new AuthServiceImpl(repository, provideUserService(repository));
    }

    private static UserService provideUserService(Repository repository) {
        return new UserServiceImpl(repository);
    }

    private static Repository provideRepository(String sourceFile) {
        return new RepositoryImpl(new File(sourceFile), provideSerializer());
    }

    private static Serializer provideSerializer() {
        return SerializerConfigurator.provideSerializer();
    }
}
