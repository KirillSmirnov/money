package com.smirnowku.money.core;

import com.smirnowku.money.core.domain.User;

class AuthServiceImpl implements AuthService {

    private final Repository repository;
    private final UserService userService;
    private boolean authenticated = false;

    AuthServiceImpl(Repository repository, UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    public boolean hasUser() {
        return repository.getUser() != null;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void createUser(String username, String password) {
        userService.save(new User(username, password));
        authenticated = true;
    }

    @Override
    public String getUsername() {
        if (!hasUser())
            throw new RuntimeException("Cannot get username: no user available");
        return repository.getUser().getUsername();
    }

    @Override
    public void authenticate(String password) {
        if (!hasUser())
            throw new RuntimeException("Cannot authenticate: no user available");
        authenticated = repository.getUser().verifyPassword(password);
    }

    @Override
    public User getUser() {
        if (!isAuthenticated())
            throw new RuntimeException("User not authenticated");
        User user = repository.getUser();
        user.setUserService(userService);
        return user;
    }
}
