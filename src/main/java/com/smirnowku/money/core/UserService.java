package com.smirnowku.money.core;

import com.smirnowku.money.core.domain.User;

public interface UserService {

    void save(User user);
}
