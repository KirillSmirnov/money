package com.smirnowku.money.serialization;

public class DeserializationException extends Exception {

    DeserializationException(Throwable cause) {
        super(cause);
    }
}
