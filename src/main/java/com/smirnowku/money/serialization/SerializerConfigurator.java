package com.smirnowku.money.serialization;

public class SerializerConfigurator {

    public static Serializer provideSerializer() {
        return new SerializerImpl();
    }
}
