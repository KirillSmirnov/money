package com.smirnowku.money.fx;

public class Layouts {

    //root
    public static final StageProperty MAIN = new StageProperty("/layout/root/main.fxml", true, true);
    public static final StageProperty LOG_IN = new StageProperty("Log In", "/layout/root/log_in.fxml", false, false);
    public static final StageProperty CREATE_USER = new StageProperty("Create User", "/layout/root/create_user.fxml", false, false);
    public static final StageProperty ADD_ACCOUNT = new StageProperty("Add Account", "/layout/root/add_account.fxml", false, false);
    public static final StageProperty ADD_RECORD = new StageProperty("/layout/root/add_record.fxml", false, false);
    public static final StageProperty CHANGE_PASSWORD = new StageProperty("Change Password", "/layout/root/change_password.fxml", false, false);

    //dialog
    public static final StageProperty CONFIRM_DIALOG = new StageProperty("/layout/dialog/confirm.fxml", false, true);
    public static final StageProperty PROMPT_DIALOG = new StageProperty("/layout/dialog/prompt.fxml", false, true);
}
