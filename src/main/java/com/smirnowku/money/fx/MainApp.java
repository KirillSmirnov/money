package com.smirnowku.money.fx;

import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {

    private static MainApp app;
    private Stage primaryStage;

    public static void main(String[] args) {
        launch(args);
    }

    public static MainApp get() {
        return app;
    }

    public MainApp() {
        MainApp.app = this;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        new StageBuilder(this.primaryStage)
                .initialize(Layouts.MAIN)
                .show();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
