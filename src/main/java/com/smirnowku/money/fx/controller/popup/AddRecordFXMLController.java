package com.smirnowku.money.fx.controller.popup;

import com.smirnowku.money.fx.controller.FXMLController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class AddRecordFXMLController extends FXMLController {

    public TextField majorUnitBox;
    public TextField minorUnitBox;
    public Label errorLabel;
    public Button actionButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        actionButton.disableProperty().bind(majorUnitBox.textProperty().isEmpty()
                .or(errorLabel.textProperty().isNotEmpty()));
        majorUnitBox.textProperty().addListener(observable -> check());
        minorUnitBox.textProperty().addListener(observable -> check());
    }

    private void check() {
        errorLabel.setText(null);
        if (!okMinorUnit(minorUnitBox.getText()))
            errorLabel.setText("Wrong minor unit format");
        if (!okMajorUnit(majorUnitBox.getText()))
            errorLabel.setText("Wrong major unit format");
    }

    private boolean okMajorUnit(String majorUnit) {
        try {
            Integer.parseInt(majorUnit);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean okMinorUnit(String minorUnit) {
        try {
            return Integer.parseInt(minorUnit) < 100;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
