package com.smirnowku.money.fx.controller.popup;

import com.smirnowku.money.core.domain.Currency;
import com.smirnowku.money.fx.controller.FXMLController;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class AddAccountFXMLController extends FXMLController {

    public TextField nameField;
    public ComboBox<Currency> currencyBox;
    public Button addButton;
    public Button cancelButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currencyBox.setItems(FXCollections.observableArrayList(Currency.values()));
        addButton.disableProperty().bind(nameField.textProperty().isEmpty()
                .or(currencyBox.getSelectionModel().selectedItemProperty().isNull()));
    }

    public void addAccount() {
        getDataSourceManager().provideAuthService().getUser()
                .addAccount(nameField.getText(), currencyBox.getValue());
        cancelButton.fire();
    }
}
