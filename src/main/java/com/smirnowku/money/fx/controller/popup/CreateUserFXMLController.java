package com.smirnowku.money.fx.controller.popup;

import com.smirnowku.money.core.AuthService;
import com.smirnowku.money.fx.Notification;
import com.smirnowku.money.fx.controller.FXMLController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class CreateUserFXMLController extends FXMLController {

    public TextField usernameField;
    public PasswordField passwordField;
    public PasswordField repeatPasswordField;
    public Label notificationLabel;
    public Button createUserButton;
    public Button cancelButton;

    private Notification notification;
    private AuthService authService;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        notification = new Notification(notificationLabel);
        authService = getDataSourceManager().provideAuthService();
        createUserButton.disableProperty().bind(usernameField.textProperty().isEmpty());
    }

    public void createUser() {
        if (!passwordField.getText().equals(repeatPasswordField.getText())) {
            notification.show("Passwords don't match");
            return;
        }
        authService.createUser(usernameField.getText(), passwordField.getText());
        cancelButton.fire();
    }
}
