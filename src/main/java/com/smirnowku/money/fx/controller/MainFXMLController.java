package com.smirnowku.money.fx.controller;

import com.smirnowku.money.core.domain.*;
import com.smirnowku.money.util.Money;
import javafx.collections.FXCollections;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class MainFXMLController extends FXMLController {

    public ListView<Account> accountsList;

    public TableView<Record> recordsTable;
    public TableColumn<Record, LocalDateTime> dateColumn;
    public TableColumn<Record, String> commentColumn;
    public TableColumn<Record, Operation> operationColumn;
    public TableColumn<Record, Money> amountColumn;
    public TableColumn<Record, Money> balanceColumn;

    public Menu accountMenu;
    public Menu settingsMenu;
    public MenuItem closeFileButton;
    public MenuItem addMoneyButton;
    public MenuItem withdrawMoneyButton;
    public MenuItem deleteRecordButton;
    public MenuItem renameAccountButton;
    public MenuItem deleteAccountButton;
    public RadioMenuItem nameSortingButton;
    public RadioMenuItem dateSortingButton;
    public RadioMenuItem currencySortingButton;

    public Label statusLabel;
    public Tooltip statusTip;
    public Label totalLabel;

    private FileController fileController;
    private AccountController accountController;
    private UserController userController;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("dateTime"));
        commentColumn.setCellValueFactory(new PropertyValueFactory<>("comment"));
        operationColumn.setCellValueFactory(new PropertyValueFactory<>("operation"));
        amountColumn.setCellValueFactory(new PropertyValueFactory<>("amount"));
        balanceColumn.setCellValueFactory(new PropertyValueFactory<>("balance"));
        commentColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        dateColumn.setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateTimeConverter()));

        accountsList.prefWidthProperty().bind(getPrimaryStage().widthProperty().multiply(0.3));
        accountsList.getSelectionModel().selectedItemProperty().addListener((observable -> refreshRecords()));

        fileController = new FileController(this::refresh);
        accountController = new AccountController(this::refreshData);
        userController = new UserController();
    }

    // File

    public void newFile() {
        fileController.openFile("New data file", false);
    }

    public void openFile() {
        fileController.openFile("Open data file", true);
    }

    public void closeFile() {
        fileController.closeFile();
    }

    // Account

    public void addMoney() {
        accountController.addMoney(getSelectedAccount());
    }

    public void withdrawMoney() {
        accountController.withdrawMoney(getSelectedAccount());
    }

    public void deleteRecord() {
        accountController.deleteRecord(getSelectedAccount());
    }

    public void addAccount() {
        accountController.addAccount();
    }

    public void renameAccount() {
        accountController.renameAccount(getSelectedAccount());
    }

    public void deleteAccount() {
        accountController.deleteAccount(getSelectedAccount());
    }

    // Settings

    public void changeAccountsSortingType() {
        User user = getDataSourceManager().provideAuthService().getUser();
        user.setAccountsSortingType(getAccountsSortingType());
        refreshData();
    }

    public void changeUsername() {
        userController.changeUsername(this::refreshUsername);
    }

    public void changePassword() {
        userController.changePassword();
    }

    // Table

    public void onCommentCommit(TableColumn.CellEditEvent<Record, String> recordStringCellEditEvent) {
        String comment = recordStringCellEditEvent.getNewValue();
        recordStringCellEditEvent.getRowValue().setComment(comment);
    }

    private void refresh() {
        boolean fileOpened = getDataSourceManager().isFileOpened();
        closeFileButton.setDisable(!fileOpened);
        accountMenu.setVisible(fileOpened);
        settingsMenu.setVisible(fileOpened);
        statusLabel.setUnderline(fileOpened);
        statusTip.setText(fileOpened ? getDataSourceManager().getSourceFile() : "No file");
        refreshUsername();
    }

    private void refreshUsername() {
        boolean fileOpened = getDataSourceManager().isFileOpened();
        statusLabel.setText(fileOpened ? getDataSourceManager().provideAuthService().getUsername() : "No file");
        refreshData();
    }

    private void refreshData() {
        if (!getDataSourceManager().isFileOpened()) {
            totalLabel.setText(null);
            accountsList.setItems(null);
            recordsTable.setItems(null);
            return;
        }
        User user = getDataSourceManager().provideAuthService().getUser();
        setAccountsSortingType(user.getAccountsSortingType());
        accountsList.setItems(FXCollections.observableList(user.getAccounts()));
        refreshRecords();
    }

    private void refreshRecords() {
        if (!getDataSourceManager().isFileOpened()) return;
        User user = getDataSourceManager().provideAuthService().getUser();
        Account account = getSelectedAccount();
        boolean accountSelected = account != null;
        boolean hasRecords = accountSelected && !account.getRecords().isEmpty();
        accountsList.refresh();
        totalLabel.setText("Total Balance: " + user.getTotalBalance());
        addMoneyButton.setDisable(!accountSelected);
        withdrawMoneyButton.setDisable(!hasRecords);
        deleteRecordButton.setDisable(!hasRecords);
        renameAccountButton.setDisable(!accountSelected);
        deleteAccountButton.setDisable(!accountSelected);
        recordsTable.setItems(accountSelected ? FXCollections.observableList(account.getRecords()) : null);
    }

    private Account getSelectedAccount() {
        return accountsList.getSelectionModel().getSelectedItem();
    }

    private void setAccountsSortingType(AccountsSortingType type) {
        nameSortingButton.setSelected(type == AccountsSortingType.NAME);
        dateSortingButton.setSelected(type == AccountsSortingType.DATE);
        currencySortingButton.setSelected(type == AccountsSortingType.CURRENCY);
    }

    private AccountsSortingType getAccountsSortingType() {
        if (nameSortingButton.isSelected()) return AccountsSortingType.NAME;
        if (dateSortingButton.isSelected()) return AccountsSortingType.DATE;
        if (currencySortingButton.isSelected()) return AccountsSortingType.CURRENCY;
        throw new RuntimeException();
    }
}
