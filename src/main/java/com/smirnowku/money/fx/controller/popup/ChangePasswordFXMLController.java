package com.smirnowku.money.fx.controller.popup;

import com.smirnowku.money.core.domain.User;
import com.smirnowku.money.fx.Notification;
import com.smirnowku.money.fx.controller.FXMLController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

import java.net.URL;
import java.util.ResourceBundle;

public class ChangePasswordFXMLController extends FXMLController {

    public PasswordField passwordField;
    public PasswordField repeatPasswordField;
    public Label notificationLabel;
    public Button cancelButton;

    private Notification notification;
    private User user;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        notification = new Notification(notificationLabel);
        user = getDataSourceManager().provideAuthService().getUser();
    }

    public void changePassword() {
        if (!passwordField.getText().equals(repeatPasswordField.getText())) {
            notification.show("Passwords don't match");
            return;
        }
        user.setPassword(passwordField.getText());
        cancelButton.fire();
    }
}
