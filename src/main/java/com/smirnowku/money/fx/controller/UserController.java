package com.smirnowku.money.fx.controller;

import com.smirnowku.money.core.domain.User;
import com.smirnowku.money.fx.Layouts;
import com.smirnowku.money.fx.StageBuilder;
import com.smirnowku.money.fx.dialog.PromptDialog;
import javafx.scene.control.Button;
import javafx.stage.Stage;

class UserController extends Controller {

    void changeUsername(Runnable refresher) {
        User user = getDataSourceManager().provideAuthService().getUser();
        new PromptDialog("Change Username", "Username:", user.getUsername(), true,
                "Save", text -> {
            user.setUsername(text);
            refresher.run();
        }).show();
    }

    void changePassword() {
        StageBuilder stageBuilder = new StageBuilder();
        Stage stage = stageBuilder.initialize(Layouts.CHANGE_PASSWORD);
        Button cancelButton = (Button) stage.getScene().lookup("#cancelButton");
        cancelButton.setOnAction(actionEvent -> stage.close());
        stageBuilder.showDialog();
    }
}
