package com.smirnowku.money.fx.controller;

import com.smirnowku.money.core.DataSourceManager;
import com.smirnowku.money.fx.MainApp;
import javafx.stage.Stage;

public abstract class Controller {

    private static final DataSourceManager DATA_SOURCE_MANAGER = new DataSourceManager();

    public void closeApp() {
        getPrimaryStage().close();
    }

    protected DataSourceManager getDataSourceManager() {
        return DATA_SOURCE_MANAGER;
    }

    Stage getPrimaryStage() {
        return MainApp.get().getPrimaryStage();
    }
}
