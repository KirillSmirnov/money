package com.smirnowku.money.fx.controller;

import com.smirnowku.money.core.domain.Account;
import com.smirnowku.money.fx.Layouts;
import com.smirnowku.money.fx.StageBuilder;
import com.smirnowku.money.fx.StageProperty;
import com.smirnowku.money.fx.dialog.ConfirmDialog;
import com.smirnowku.money.fx.dialog.PromptDialog;
import com.smirnowku.money.util.Money;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

class AccountController extends Controller {

    private interface OnAddRecordListener {

        String onAddRecord(String comment, int majorUnit, int minorUnit);
    }

    private final Runnable refresher;

    AccountController(Runnable refresher) {
        this.refresher = refresher;
    }

    void addMoney(Account account) {
        startAddRecordPopup("Add Money", "Add", account, (comment, majorUnit, minorUnit) -> {
            account.addMoney(comment, new Money(majorUnit, minorUnit));
            return null;
        });
    }

    void withdrawMoney(Account account) {
        startAddRecordPopup("Withdraw Money", "Withdraw", account, (comment, majorUnit, minorUnit) ->
                account.withdrawMoney(comment, new Money(majorUnit, minorUnit)) ? null : "Not enough money");
    }

    void deleteRecord(Account account) {
        new ConfirmDialog("Delete Last Record", "Are you sure you want to delete last record of account " +
                account.getName() + "?", "Delete", () -> {
            account.deleteLastRecord();
            refresher.run();
        }).show();
    }

    void addAccount() {
        StageBuilder stageBuilder = new StageBuilder();
        cancelablePopup(stageBuilder, Layouts.ADD_ACCOUNT);
        stageBuilder.showDialog();
    }

    void renameAccount(Account account) {
        new PromptDialog("Rename Account", "Name:", account.getName(), true,
                "Save", text -> {
            account.setName(text);
            refresher.run();
        }).show();
    }

    void deleteAccount(Account account) {
        new ConfirmDialog("Delete Account", "Are you sure you want to delete account " +
                account.getName() + "?", "Delete", () -> {
            account.delete();
            refresher.run();
        }).show();
    }

    private void startAddRecordPopup(String title, String actionBarName, Account account, OnAddRecordListener listener) {
        StageBuilder stageBuilder = new StageBuilder();
        Stage stage = cancelablePopup(stageBuilder, Layouts.ADD_RECORD);
        stage.setTitle(title);

        TextField majorUnitBox = (TextField) stage.getScene().lookup("#majorUnitBox");
        TextField minorUnitBox = (TextField) stage.getScene().lookup("#minorUnitBox");
        TextField commentBox = (TextField) stage.getScene().lookup("#commentBox");
        Label majorUnitLabel = (Label) stage.getScene().lookup("#majorUnitLabel");
        Label minorUnitLabel = (Label) stage.getScene().lookup("#minorUnitLabel");
        Label errorLabel = (Label) stage.getScene().lookup("#errorLabel");
        Button actionButton = (Button) stage.getScene().lookup("#actionButton");
        Button cancelButton = (Button) stage.getScene().lookup("#cancelButton");

        majorUnitLabel.setText(account.getCurrency().getMajorUnitSymbol());
        minorUnitLabel.setText(account.getCurrency().getMinorUnitSymbol());

        actionButton.setText(actionBarName);
        actionButton.setOnAction(actionEvent -> {
            int majorUnit = Integer.parseInt(majorUnitBox.getText());
            int minorUnit = Integer.parseInt(minorUnitBox.getText());
            String error = listener.onAddRecord(commentBox.getText(), majorUnit, minorUnit);
            if (error == null) cancelButton.fire();
            else errorLabel.setText(error);
        });

        stageBuilder.showDialog();
    }

    private Stage cancelablePopup(StageBuilder stageBuilder, StageProperty stageProperty) {
        Stage stage = stageBuilder.initialize(stageProperty);
        ((Button) stage.getScene().lookup("#cancelButton"))
                .setOnAction(actionEvent -> {
                    stage.close();
                    refresher.run();
                });
        return stage;
    }
}
