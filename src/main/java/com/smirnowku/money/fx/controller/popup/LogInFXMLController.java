package com.smirnowku.money.fx.controller.popup;

import com.smirnowku.money.core.AuthService;
import com.smirnowku.money.fx.Notification;
import com.smirnowku.money.fx.controller.FXMLController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;

import java.net.URL;
import java.util.ResourceBundle;

public class LogInFXMLController extends FXMLController {

    public Label titleLabel;
    public PasswordField passwordField;
    public Label notificationLabel;
    public Button cancelButton;

    private Notification notification;
    private AuthService authService;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        notification = new Notification(notificationLabel);
        authService = getDataSourceManager().provideAuthService();
        titleLabel.setText(authService.getUsername());
    }

    public void logIn() {
        authService.authenticate(passwordField.getText());
        if (authService.isAuthenticated()) cancelButton.fire();
        else notification.show("Wrong password");
    }
}
