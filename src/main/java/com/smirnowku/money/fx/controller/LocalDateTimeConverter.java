package com.smirnowku.money.fx.controller;

import javafx.util.StringConverter;

import java.time.LocalDateTime;

class LocalDateTimeConverter extends StringConverter<LocalDateTime> {

    @Override
    public String toString(LocalDateTime localDateTime) {
        return String.format("%d-%02d-%02d %02d:%02d",
                localDateTime.getYear(),
                localDateTime.getMonthValue(),
                localDateTime.getDayOfMonth(),
                localDateTime.getHour(),
                localDateTime.getMinute());
    }

    @Override
    public LocalDateTime fromString(String s) {
        return null;
    }
}
