package com.smirnowku.money.fx.controller;

import com.smirnowku.money.core.AuthService;
import com.smirnowku.money.fx.Layouts;
import com.smirnowku.money.fx.StageBuilder;
import com.smirnowku.money.fx.StageProperty;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

class FileController extends Controller {

    private final Runnable refresher;

    FileController(Runnable refresher) {
        this.refresher = refresher;
        if (getDataSourceManager().isFileOpened()) authenticateOnLaunch();
        else refresher.run();
    }

    void closeFile() {
        getDataSourceManager().closeFile();
        refresher.run();
    }

    void openFile(String title, boolean fileExists) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        File sourceFile = fileExists ? fileChooser.showOpenDialog(getPrimaryStage())
                : fileChooser.showSaveDialog(getPrimaryStage());
        if (sourceFile != null) {
            getDataSourceManager().openFile(sourceFile.getAbsolutePath());
            authenticate(fileExists);
        }
    }

    private void authenticate(boolean fileExists) {
        AuthService authService = getDataSourceManager().provideAuthService();
        if (authService.hasUser() && fileExists) start(Layouts.LOG_IN);
        else start(Layouts.CREATE_USER);
    }

    private void authenticateOnLaunch() {
        AuthService authService = getDataSourceManager().provideAuthService();
        if (authService.hasUser()) start(Layouts.LOG_IN);
        else checkAuthenticated();
    }

    private void start(StageProperty stageProperty) {
        StageBuilder stageBuilder = new StageBuilder();
        Stage stage = stageBuilder.initialize(stageProperty);
        stage.setOnCloseRequest(windowEvent -> checkAuthenticated());
        Button cancelButton = (Button) stage.getScene().lookup("#cancelButton");
        cancelButton.setOnAction(actionEvent -> {
            stage.close();
            checkAuthenticated();
        });
        stageBuilder.showDialog();
    }

    private void checkAuthenticated() {
        AuthService authService = getDataSourceManager().provideAuthService();
        if (!authService.isAuthenticated()) getDataSourceManager().closeFile();
        refresher.run();
    }
}
