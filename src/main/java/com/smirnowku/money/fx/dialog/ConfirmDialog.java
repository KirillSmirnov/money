package com.smirnowku.money.fx.dialog;

import com.smirnowku.money.fx.Layouts;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ConfirmDialog extends Dialog {

    public interface OnConfirmButtonClick {

        void onClick();
    }

    public ConfirmDialog(String title, String text, String confirmButtonName, OnConfirmButtonClick onClick) {
        super(title, Layouts.CONFIRM_DIALOG);

        Label textLabel = (Label) stage.getScene().lookup("#textLabel");
        Button okButton = (Button) stage.getScene().lookup("#okButton");
        Button cancelButton = (Button) stage.getScene().lookup("#cancelButton");

        textLabel.setText(text);
        okButton.setText(confirmButtonName);
        okButton.setOnAction(event -> {
            onClick.onClick();
            stage.close();
        });
        cancelButton.setOnAction(event -> stage.close());
    }
}
