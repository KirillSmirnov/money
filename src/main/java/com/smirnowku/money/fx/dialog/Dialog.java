package com.smirnowku.money.fx.dialog;

import com.smirnowku.money.fx.StageBuilder;
import com.smirnowku.money.fx.StageProperty;
import javafx.stage.Stage;

abstract class Dialog {

    private final StageBuilder stageBuilder;
    final Stage stage;

    Dialog(String title, StageProperty dialogStageProperty) {
        stageBuilder = new StageBuilder();
        stage = stageBuilder.initialize(dialogStageProperty);
        stage.setTitle(title);
    }

    public void show() {
        stageBuilder.showDialog();
    }
}
