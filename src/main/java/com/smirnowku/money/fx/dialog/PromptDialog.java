package com.smirnowku.money.fx.dialog;

import com.smirnowku.money.fx.Layouts;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class PromptDialog extends Dialog {

    public interface OnPositiveButtonClick {

        void onClick(String text);
    }

    public PromptDialog(String title, String label, String text, boolean preventEmptyText, String positiveButtonName, OnPositiveButtonClick onClick) {
        super(title, Layouts.PROMPT_DIALOG);

        Label textLabel = (Label) stage.getScene().lookup("#textLabel");
        TextField textField = (TextField) stage.getScene().lookup("#textField");
        Button okButton = (Button) stage.getScene().lookup("#okButton");
        Button cancelButton = (Button) stage.getScene().lookup("#cancelButton");

        textLabel.setText(label);
        textField.setText(text);
        okButton.setText(positiveButtonName);
        okButton.setOnAction(event -> {
            onClick.onClick(textField.getText());
            stage.close();
        });
        cancelButton.setOnAction(event -> stage.close());
        if (preventEmptyText)
            okButton.disableProperty().bind(textField.textProperty().isEmpty());
    }
}
