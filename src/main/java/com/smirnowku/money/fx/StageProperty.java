package com.smirnowku.money.fx;

import javafx.scene.image.Image;

public class StageProperty {

    private static final Image APP_ICON = new Image("/icon/money.png");
    private static final String APP_TITLE = "Money";

    private final String title;
    private final String sceneLayoutPath;
    private final boolean resizable;
    private final boolean centered;

    StageProperty(String title, String sceneLayoutPath, boolean resizable, boolean centered) {
        this.title = title;
        this.sceneLayoutPath = sceneLayoutPath;
        this.resizable = resizable;
        this.centered = centered;
    }

    StageProperty(String sceneLayoutPath, boolean resizable, boolean centered) {
        this(APP_TITLE, sceneLayoutPath, resizable, centered);
    }

    Image getIcon() {
        return APP_ICON;
    }

    String getTitle() {
        return title;
    }

    String getSceneLayoutPath() {
        return sceneLayoutPath;
    }

    boolean isResizable() {
        return resizable;
    }

    boolean isCentered() {
        return centered;
    }
}
