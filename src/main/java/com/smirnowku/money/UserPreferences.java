package com.smirnowku.money;

import java.util.prefs.Preferences;

public class UserPreferences {

    private static final String SOURCE_FILE = "SOURCE_FILE";

    private final Preferences preferences = Preferences.userNodeForPackage(getClass());

    public String getSourceFile() {
        return preferences.get(SOURCE_FILE, null);
    }

    public void setSourceFile(String sourceFile) {
        setValue(SOURCE_FILE, sourceFile);
    }

    private void setValue(String key, String value) {
        if (value == null) preferences.remove(key);
        else preferences.put(key, value);
    }
}
