package com.smirnowku.money.util;

public class NullChecker {

    public static void ensureNotNull(Object object) {
        if (isNull(object))
            throw new NullPointerException("object is null");
    }

    public static void ensureNotNullOrEmpty(String s) {
        if (isNullOrEmpty(s))
            throw new IllegalArgumentException("string is empty");
    }

    private static boolean isNull(Object object) {
        return object == null;
    }

    private static boolean isNullOrEmpty(String s) {
        return isNull(s) || s.isEmpty();
    }
}
