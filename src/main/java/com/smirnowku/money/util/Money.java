package com.smirnowku.money.util;

import java.io.Serializable;

public class Money implements Serializable, Comparable<Money> {

    private final long majorUnit;
    private final int minorUnit;

    public static Money zero() {
        return new Money(0);
    }

    public Money(long majorUnit, int minorUnit) {
        checkMajorUnit(majorUnit);
        checkMinorUnit(minorUnit);
        this.majorUnit = majorUnit;
        this.minorUnit = minorUnit;
    }

    private Money(long value) {
        this(value / 100, (int) (value % 100));
    }

    public Money add(Money other) {
        return new Money(this.value() + other.value());
    }

    public Money subtract(Money other) {
        if (compareTo(other) < 0)
            throw new RuntimeException("Unsupported subtraction");
        return new Money(this.value() - other.value());
    }

    public long getMajorUnit() {
        return majorUnit;
    }

    public int getMinorUnit() {
        return minorUnit;
    }

    private long value() {
        return majorUnit * 100 + minorUnit;
    }

    private static void checkMajorUnit(long majorUnit) {
        if (majorUnit < 0)
            throw new RuntimeException("Negative major unit");
    }

    private static void checkMinorUnit(int minorUnit) {
        if (minorUnit < 0)
            throw new RuntimeException("Negative minor unit");
        if (minorUnit >= 100)
            throw new RuntimeException("Minor unit cannot be grater or equal than 100");
    }

    private static String formatMajorUnit(long majorUnit) {
        char[] majorUnitDigits = ("" + majorUnit).toCharArray();
        StringBuilder result = new StringBuilder();
        int count = 0;
        for (int i = majorUnitDigits.length - 1; i >= 0; --i) {
            result.insert(0, majorUnitDigits[i]);
            if (++count == 3 && i > 0) {
                result.insert(0, ' ');
                count = 0;
            }
        }
        return result.toString();
    }

    @Override
    public int compareTo(Money money) {
        return Long.compare(value(), money.value());
    }

    @Override
    public String toString() {
        return String.format("%s.%02d", formatMajorUnit(majorUnit), minorUnit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return compareTo((Money) o) == 0;
    }

    @Override
    public int hashCode() {
        int result = (int) (majorUnit ^ (majorUnit >>> 32));
        result = 31 * result + minorUnit;
        return result;
    }
}
